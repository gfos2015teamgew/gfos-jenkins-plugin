/*
 *  Lädt einen Wert aus gespeicherten Einstellungen,
 *  mit Support für Standardeinstellungen
 */
var settings_getValue = function(key, defaultValue, callback) {
    
    chrome.storage.sync.get(key, function(data) {
        var item = data[key];
        
        /*
        Wenn nicht gefunden, gib den Standardwert zurück
        */
        if (typeof item === 'undefined')
            callback(defaultValue);
        else callback(item);
    });
};

/*
 *  Speichert einen Wert in den Einstellungen
 */
function settings_setValue(key, value) {
    //Erzeuge das schreibbare Objekt
    var dataObject = {};
    dataObject[key] = value;
    
    //Speicher das Objekt
    chrome.storage.sync.set(dataObject);
}