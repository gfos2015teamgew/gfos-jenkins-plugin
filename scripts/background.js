/**
 *  Hintergrundskript, dass das Icon der BrowserAction beeinflusst
 *
 *  Also das, was in der Aufgabenstellung als "Aggregation aller Projekte" bezeichnet wurde
 */

//Starte das Interval
settings_getValue("refreshInterval", "60", function (refreshInterval) {
    window.setInterval(function() { iconRefresh(); }, refreshInterval*1000);
});

//Manuell schon einmal anstoßen
iconRefresh();
    
/**
 *  Funktion des Refresh-Background-Cycles
 */
function iconRefresh() {
    
    //Hol die Liste der Jobs
    jenkins_getJobs(function(data) {
        var blue = 0;
        var red = 0;
    
        //Für jeden Job: Wie ist der Status?
        for(var i = 0; i < data.length; i++) {
            if(data[i].color == "blue") {
                //Erfolgreich
                blue++;
            }else if(data[i].color == "red") {
                //Fehlgeschlagen
                red++;
            }
        }
        
        //Wenn mehr Erfolgreiche als Fehlgeschlagene Builds vorhanden sind, soll das Icon dementsprechend aussehen
        if (blue == 0 && red == 0) {
            
            //Neutraler Jenkins / Hudson
            chrome.browserAction.setIcon({path: "src/logo.png"});
        }else if(blue > red) {
            
            //Cute Jenkins
            chrome.browserAction.setIcon({path: "src/jenkins-thpr.png"});
        } else {
            
            //Fire Jenkins / Devil
            chrome.browserAction.setIcon({path: "src/fire-jenkins.png"});
        }
        
        //Zeige als Hover-Popup an, wie viele erfolgreiche und fehlgeschlagene Jobs
        chrome.browserAction.setTitle({title: blue + " erfolgreiche, " + red + " nicht erfolgreiche Projekte"});
        
        //Zeig die Anzahl der fehlgeschlagenen als Badge über dem Icon an
        chrome.browserAction.setBadgeText({text: red.toString()});
    });
}