/**
 *  Hauptscript der Extension
 *  Lädt Daten, EventHandler der Navigation
 */
$(document).ready(function() {
    
    //Lade beim Start die Ansicht, die beim letzten Schließen geöffnet war
    settings_getValue("lastView", "home", function(lastView) {
        loadView(lastView);
    });
    
    //Query Selector für selbst gebastelte <a>Links</a>, die bei uns
    //so aussehen: <span class="link" data-target="settings">Link</span>
    var linkSelector = "span.link";
    
    //Event auf alle Links anwenden
    $("body").delegate(linkSelector, "click", function() {
        var element = $(this);
        
        //Zielview
        var target = element.data("target");
        
        //Laden
        loadView(target);
    });
    
    
    //Beim Klicken des Home-Buttons in der Navbar:
    $("img.navbar-icon").click(function() {
        
        //Startseite laden
        loadView("home");
    });
    
    
    //Beim Klicken des Refresh-Buttons in der Navbar: 
    $("img.navbar-refresh").click(function() {
        
        //Teste die Verbindung und lade ggf. die Daten neu
        refresh();
    })
    
    
    //Beim Klicken des Zahnrads in der Navbar:
    $("img.navbar-settings").click(function() {
        
        //Einstellungen laden
        loadView("settings");
    });
    

    //Starte den Refresh Cycle
    refresh(true); //Direkt die Verbindung testen
    //Interval in Sekunden
    settings_getValue("refreshInterval", 60, function(interval) {
        
        window.setInterval(function() { refresh(); }, interval*1000);
    });
    
});